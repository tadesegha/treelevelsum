﻿$src = & csc.exe /t:library /out:bin\treeLevelSum.dll /recurse:src\*.cs /nologo

if ( $LASTEXITCODE -ne 0 ){
  return $src
}

$test = & csc.exe /t:library /out:bin\treeLevelSumTests.dll /reference:test\nunit.framework\bin\net-4.5\nunit.framework.dll,bin\treeLevelSum.dll /recurse:test\*.cs

if ( $LASTEXITCODE -ne 0 ){
  return $test
}

& ./test/nunit.console/nunit3-console.exe ./bin/treeLevelSumTests.dll
