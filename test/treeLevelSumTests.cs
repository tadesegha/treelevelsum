using TreeLevelSum;
using NUnit.Framework;

namespace TreeLevelSum.Tests
{
    [TestFixture]
    public class TreeLevelSumTests
    {
        [TestCase("(0(5(6()())(14()(9()())))(7(1()())(23()())))", 2, 44)]
        [TestCase("(3(3()())(1()()))", 1, 4)]
        [TestCase("(2()())", 100, 0)]
        public void ShouldGiveTheRightAnswer(string tree, int level, int expected)
        {
            var sut = new TreeLevelSum();
            Assert.AreEqual(expected, sut.treeLevelSum(tree, level));
        }
    }
}
