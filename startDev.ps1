﻿function run {
  param ($path = (read-host 'Path'),
      [ scriptblock ] $action = (read-host 'Action'),
      [ array ] $excludedFiles = @())

  $global:excludedFiles = $excludedFiles
  $path = (convert-path $path)

  $fileWatcher = New-Object -TypeName System.IO.FileSystemWatcher
  $fileWatcher.Path = $path
  $fileWatcher.IncludeSubdirectories = $true

  Register-ObjectEvent -InputObject $fileWatcher -EventName 'Error' -Action {
      $global:monitorFilesError = $event
      throw 'An error was thrown by FileSystemWatcher. Navigate $monitorFilesError to view details about the error'
  } | Out-Null

  $validateEvent = {
  $ignoredFiles = @('.git')

  foreach ($ignoredFile in @('.git')){
          if ($event.SourceEventArgs.FullPath.contains($ignoredFile)) {
              return
          }
  }

      foreach ($excludedFile in $global:excludedFiles) {
          if ($event.SourceEventArgs.FullPath.EndsWith($excludedFile)) {
              return
          }
      }

      $file = get-item $event.SourceEventArgs.FullPath

      if ($file -is [ System.IO.DirectoryInfo ]) {
          return
      }

      if ($event.SourceEventArgs.ChangeType -eq 'Created') {
          if ($file.CreationTime -lt [ DateTime ]::Now.AddSeconds(-5)) {
              return
          }
      }

      if (-not $global:lastHandledEvent) {
          $global:lastHandledEvent = @{
              Time = [System.DateTime]::Now.AddDays(-1);
          }
      }

      $eventRecentlyFired = (($global:lastHandledEvent.Path -eq $file) -and [System.DateTime]::Now -lt ($global:lastHandledEvent.Time.AddSeconds(3)))

      if ($eventRecentlyFired) {
          return
      }
  }

  $cleanup = {
      $global:lastHandledEvent = @{
          Time = [System.DateTime]::Now;
          Path = $file
      }
  }

  Register-ObjectEvent -InputObject $fileWatcher -EventName 'Changed' -Action ([scriptblock]::create($validateEvent.ToString() + $action.ToString() + $cleanup.ToString())) | Out-Null
  Register-ObjectEvent -InputObject $fileWatcher -EventName 'Created' -Action ([scriptblock]::create($validateEvent.ToString() + $action.ToString() + $cleanup.ToString())) | Out-Null
}

$action = [scriptblock]::create("clear; & '$PSScriptRoot\runtests.ps1' | Out-Host")
run -path "$PSScriptRoot\src" -action $action
run -path "$PSScriptRoot\test" -action $action

write-host 'monitoring src and test directories for change. unit tests will be run with each change'
