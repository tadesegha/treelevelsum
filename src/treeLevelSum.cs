using System;

namespace TreeLevelSum
{
    public class TreeLevelSum
    {
        public int treeLevelSum(string tree, int level)
        {
            var formattedTree = tree.Replace("(", " ( ").Replace(")", " ) ").Split(' ');
            var sum = 0;
            var treeLevel = -1;

            foreach (var item in formattedTree)
            {
                var temp = item.Trim();
                if (temp == string.Empty)
                    continue;

                if (item == "(")
                    treeLevel++;
                else if (item == ")")
                    treeLevel--;
                else if (level == treeLevel)
                    sum += Convert.ToInt32(item);
            }

            return sum;
        }
    }
}
